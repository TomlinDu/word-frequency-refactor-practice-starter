import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String DELIMITER = "\\s+";

    public String calculateWordFrequency(String inputString) {
        try {
            List<Word> wordList = getWordList(transfromToWordList(inputString));
            wordList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
            return getStringJoiner(wordList).toString();
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private StringJoiner getStringJoiner(List<Word> wordList) {
        StringJoiner joiner = new StringJoiner("\n");
        wordList.stream()
                .map(word -> word.getWord() + " " + word.getWordCount())
                .forEach(joiner::add);
        return joiner;
    }

    private List<Word> getWordList(List<Word> wordList) {
        Map<String, Integer> sameWordMap = new HashMap<>();
        wordList.forEach(word -> sameWordMap.put(word.getWord(), sameWordMap.getOrDefault(word.getWord(), 0) + 1));
        List<Word> sameWordList = new ArrayList<>();
        sameWordMap.forEach((key, value) -> sameWordList.add(new Word(key, value)));
        return sameWordList;
    }

    private List<Word> transfromToWordList(String inputString) {
        String[] words = inputString.split(DELIMITER);
        return Arrays.stream(words).map(word -> new Word(word, 1)).collect(Collectors.toList());
    }

}
